package com.shchipunov.workshop;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tritonus.share.sampled.file.TAudioFileFormat;

/**
 * Presents an utility that allows to do various operations related to audio
 * files
 * 
 * @author Shchipunov Stanislav
 */
public class AudioUtility {

	private static final Logger LOGGER = LoggerFactory.getLogger(AudioUtility.class);

	private AudioUtility() {

	}

	public static long getDurationOfAudioFile(Path pathToFile) {
		try {
			try (AudioInputStream audioInputStream = AudioSystem
					.getAudioInputStream(Files.newInputStream(pathToFile, StandardOpenOption.READ))) {
				AudioFileFormat audioFileFormat = AudioSystem.getAudioFileFormat(audioInputStream);
				AudioFormat audioFormat = audioInputStream.getFormat();
				int frameSize;
				if (audioFileFormat instanceof TAudioFileFormat) {
					Map<String, Object> properties = ((TAudioFileFormat) audioFileFormat).properties();
					Long durationInMicroseconds = (Long) properties.get("duration");
					if (durationInMicroseconds != null) {
						return TimeUnit.SECONDS.convert(durationInMicroseconds, TimeUnit.MICROSECONDS);
					}
					frameSize = Optional.of((Integer) properties.get("mp3.framesize.bytes"))
							.orElseGet(() -> audioFormat.getFrameSize());
				} else {
					frameSize = audioFormat.getFrameSize();
				}
				int bytesPerSecond = frameSize * Math.round(audioFormat.getFrameRate());
				int headersLength = 3;
				long durationInSeconds = Math.round((double) pathToFile.toFile().length() / bytesPerSecond)
						- headersLength;
				LOGGER.debug("Duration in seconds: {}", durationInSeconds);
				return durationInSeconds;
			}
		} catch (IOException | UnsupportedAudioFileException exception) {
			throw new UnsupportedOperationException(
					String.format("Could not process audio file: %s", pathToFile.toFile().getName()), exception);
		}
	}

	public static void cutFragmentFromAudioFile(Path pathToInputFile, Path pathToOutputFile, int startInSeconds,
			int durationInSeconds) {
		try {
			LOGGER.trace("Start in seconds: {}", startInSeconds);
			LOGGER.trace("Duration in seconds: {}", durationInSeconds);
			int bytesPerSecond;
			try (AudioInputStream audioInputStream = AudioSystem
					.getAudioInputStream(Files.newInputStream(pathToInputFile, StandardOpenOption.READ))) {
				AudioFileFormat audioFileFormat = AudioSystem.getAudioFileFormat(audioInputStream);
				AudioFormat audioFormat = audioInputStream.getFormat();
				int frameSize;
				if (audioFileFormat instanceof TAudioFileFormat) {
					Map<String, Object> properties = ((TAudioFileFormat) audioFileFormat).properties();
					frameSize = Optional.of((Integer) properties.get("mp3.framesize.bytes"))
							.orElseGet(() -> audioFormat.getFrameSize());
				} else {
					frameSize = audioFormat.getFrameSize();
				}
				LOGGER.trace("Frame size in bytes: {}", frameSize);
				bytesPerSecond = frameSize * Math.round(audioFormat.getFrameRate());
			}
			try (BufferedInputStream bufferedInputStream = new BufferedInputStream(
					Files.newInputStream(pathToInputFile, StandardOpenOption.READ))) {
				long bytesSkipped = bufferedInputStream.skip(startInSeconds * bytesPerSecond);
				LOGGER.trace("Number of bytes to be skipped: {}", startInSeconds * bytesPerSecond);
				LOGGER.trace("Bytes skipped : {}", bytesSkipped);

				int sizeOfRequiredFragment = durationInSeconds * bytesPerSecond;
				LOGGER.trace("Size of required fragment: {}", sizeOfRequiredFragment);
				byte[] requiredFragment = new byte[sizeOfRequiredFragment];
				int bytesRead = bufferedInputStream.read(requiredFragment);
				LOGGER.trace("Bytes read: {}", bytesRead);

				Files.copy(bufferedInputStream, pathToOutputFile, StandardCopyOption.REPLACE_EXISTING);
			}
		} catch (IOException | UnsupportedAudioFileException exception) {
			throw new UnsupportedOperationException(
					String.format("Could not process audio file: %s", pathToInputFile.toFile().getName()), exception);
		}
	}

	public static void convertAudioFileToWAV(Path pathToInputFile, Path pathToOutputFile) {
		try {
			try (AudioInputStream originalAudioInputStream = AudioSystem
					.getAudioInputStream(Files.newInputStream(pathToInputFile, StandardOpenOption.READ))) {
				AudioFormat inputAudioFormat = originalAudioInputStream.getFormat();
				AudioFormat outputAudioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
						inputAudioFormat.getSampleRate(), 16, inputAudioFormat.getChannels(),
						inputAudioFormat.getChannels() * 2, inputAudioFormat.getSampleRate(), false);
				AudioFileFormat audioFileFormat = AudioSystem.getAudioFileFormat(originalAudioInputStream);
				LOGGER.trace("Type: {}", audioFileFormat.getType());
				LOGGER.trace("Byte lenght: {}", audioFileFormat.getByteLength());
				LOGGER.trace("Frame lenght: {}", audioFileFormat.getFrameLength());
				LOGGER.trace("Channels: {}", inputAudioFormat.getChannels());
				LOGGER.trace("Encoding: {}", inputAudioFormat.getEncoding());
				LOGGER.trace("Frame rate: {}", inputAudioFormat.getFrameRate());
				LOGGER.trace("Frame size: {}", inputAudioFormat.getFrameSize());
				LOGGER.trace("Sample rate: {}", inputAudioFormat.getSampleRate());
				LOGGER.trace("Sample size in bits: {}", inputAudioFormat.getSampleSizeInBits());
				LOGGER.trace("Big endian: {}", inputAudioFormat.isBigEndian());

				try (AudioInputStream convertedAudioInputStream = AudioSystem.getAudioInputStream(outputAudioFormat,
						originalAudioInputStream)) {
					int bytesWritten = AudioSystem.write(convertedAudioInputStream, AudioFileFormat.Type.WAVE,
							pathToOutputFile.toFile());
					LOGGER.trace("Bytes written: {}", bytesWritten);
				}
			}
		} catch (IOException | UnsupportedAudioFileException exception) {
			throw new UnsupportedOperationException(
					String.format("Could not process audio file: %s", pathToInputFile.toFile().getName()), exception);
		}
	}

	public static void cutFragmentFromWAV(Path pathToInputFile, Path pathToOutputFile, int startInSeconds,
			int durationInSeconds) {
		try {
			try (AudioInputStream originalAudioInputStream = AudioSystem
					.getAudioInputStream(Files.newInputStream(pathToInputFile, StandardOpenOption.READ))) {
				AudioFormat inputAudioFormat = originalAudioInputStream.getFormat();
				int bytesPerSecond = inputAudioFormat.getFrameSize() * Math.round(inputAudioFormat.getFrameRate());
				long bytesSkipped = originalAudioInputStream.skip(startInSeconds * bytesPerSecond);
				LOGGER.trace("Number of bytes to be skipped: {}", startInSeconds * bytesPerSecond);
				LOGGER.trace("Bytes skipped : {}", bytesSkipped);

				int sizeOfRequiredFragment = durationInSeconds * Math.round(inputAudioFormat.getFrameRate());
				LOGGER.trace("Size of required fragment: {}", sizeOfRequiredFragment);
				try (AudioInputStream outputAudioInputStream = new AudioInputStream(originalAudioInputStream,
						inputAudioFormat, sizeOfRequiredFragment)) {
					AudioFileFormat audioFileFormat = AudioSystem.getAudioFileFormat(originalAudioInputStream);
					int bytesWritten = AudioSystem.write(outputAudioInputStream, audioFileFormat.getType(),
							pathToOutputFile.toFile());
					LOGGER.trace("Bytes written: {}", bytesWritten);
				}
			}
		} catch (IOException | UnsupportedAudioFileException exception) {
			throw new UnsupportedOperationException(
					String.format("Could not process audio file: %s", pathToInputFile.toFile().getName()), exception);
		}
	}
}
